import React from 'react'
import './portfolio.css'
import IMG1 from '../../assets/portfolio1.png'
import IMG2 from '../../assets/portfolio2.png'
import IMG3 from '../../assets/portfolio3.png'
import IMG4 from '../../assets/portfolio4.png'
import IMG5 from '../../assets/portfolio5.png'
import IMG6 from '../../assets/portfolio6.png'


const data = [
  {
    id: 1,
    image: IMG1,
    title: 'Site e-commerce et Boutique en ligne Bonmarche.mg',
    github: 'https://github.com',
    demo: 'https://bonmarche.mg'
  },
  {
    id: 2,
    image: IMG2,
    title: 'Application mobile version android Bonmarche.mg',
    github: 'https://github.com',
    demo: 'https://play.google.com/store/apps/details?id=com.app.bonmarche&hl=fr&gl=US'
  },
  {
    id: 3,
    image: IMG3,
    title: 'Site vitrine pour labooi| laboratoire de prothèse dentaires',
    github: 'https://github.com',
    demo: 'https://labooi.com'
  },
  {
    id: 4,
    image: IMG4,
    title: 'Site vitrine labocast Reunion| laboratoire de prothèse dentaire',
    github: 'https://github.com',
    demo: 'https://labocast-reunion.fr'
  },
  {
    id: 5,
    image: IMG5,
    title: 'My Shoap project with angular',
    github: 'https://github.com',
    demo: 'https://gitlab.com/herizo-fanomezana/react-frontend'
  },
  {
    id: 6,
    image: IMG6,
    title: 'Projet Portfolio avec React',
    github: 'https://github.com',
    demo: 'https://gitlab.com/herizo-fanomezana/react-portfolio'
  },

]

const Portfolio = () => {
  return (
    <section id='portfolio'>
      <h5>Mes recentes Realisations</h5>
      <h2>Portfolio</h2>
      <div className="container portfolio__container">
        {
          data.map(({id, image, title, github, demo}) =>{
            return(
              <article key={id} className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={image} alt={title}/>

          </div>
          <h3>{title}</h3>
          <div className="portfolio__item-cta">
          <a href={github} className='btn' target='_blank'>Github</a>
          <a href={demo} className='btn btn-primary' target='_blank'>Demo</a>

          </div>
          
        </article>
            )
          })
        }
        

        {/* <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG2} alt=""/>

          </div>
          <h3>BonMarche Mobile Project</h3>
          <div className="portfolio__item-cta">
          <a href="https://gitlab.com/bonmarche" className='btn' target='_blank'>Github</a>
          <a href="https://www.bonmarche.mg" className='btn btn-primary' target='_blank'>Demo</a>

          </div>
          
        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG3} alt=""/>

          </div>
          <h3>Labooi Project</h3>
          <div className="portfolio__item-cta">
          <a href="https://gitlab.com/labooi" className='btn' target='_blank'>Github</a>
          <a href="https://www.bonmarche.mg" className='btn btn-primary' target='_blank'>Demo</a>

          </div>
          
        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG4} alt=""/>

          </div>
          <h3>Labocast Reunion Project</h3>
          <div className="portfolio__item-cta">
          <a href="https://gitlab.com/labocast" className='btn' target='_blank'>Github</a>
          <a href="https://www.bonmarche.mg" className='btn btn-primary' target='_blank'>Demo</a>

          </div>
          
        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG5} alt=""/>

          </div>
          <h3>My Shop Project</h3>
          <div className="portfolio__item-cta">
          <a href="https://gitlab.com/herizo-fanomezana/react-frontend" className='btn' target='_blank'>Github</a>
          <a href="https://www.bonmarche.mg" className='btn btn-primary' target='_blank'>Demo</a>

          </div>
          
        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG6} alt=""/>

          </div>
          <h3>Portfolio Project</h3>
          <div className="portfolio__item-cta">
          <a href="https://gitlab.com/herizo-fanomezana/react-portfolio" className='btn' target='_blank'>Github</a>
          <a href="https://www.bonmarche.mg" className='btn btn-primary' target='_blank'>Demo</a>

          </div>
        
          
        </article> */}
      </div>
    </section>
  )
}

export default Portfolio