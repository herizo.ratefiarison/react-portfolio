import React from 'react'
import './footer.css'
import {BsFacebook, BsInstagram, BsLinkedin, BsTwitter} from 'react-icons/bs'

const Footer = () => {
  return (
    <footer>
      <a href="#" className='footer__logo'>Habaka</a>
      <ul className='permalinks'>
        <li><a href="#">Accueil</a></li>
        <li><a href="#about">A Propos de Moi</a></li>
        <li><a href="#Experiences">Mes Experiences</a></li>
        <li><a href="#services">Mes Services</a></li>
        <li><a href="#portfolio">Mes réalisations</a></li>
        <li><a href="#testimonials">Temoignages</a></li>
        <li><a href="#contact">Me contacter</a></li>
      </ul>
      <div className="footer__socials">
        <a href="https://facebook.com/herizo.itstation"><BsFacebook /></a>
        <a href="https://instagram.com"><BsInstagram/></a>
        <a href="https://linkedin.com"><BsLinkedin/></a>
        <a href="https://twitter.com"><BsTwitter/></a>

      </div>
      <div className="footer__copyright">
        <small>&copy;Habaka 2022 </small>
      </div>
    </footer>
  )
}

export default Footer