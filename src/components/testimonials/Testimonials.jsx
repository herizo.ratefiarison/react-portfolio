import React from 'react'
import './testimonials.css'
import AVTR1 from '../../assets/Avatar1.png'
import AVTR2 from '../../assets/Avatar2.png'
import AVTR3 from '../../assets/Avatar3.png'
import AVTR4 from '../../assets/Avatar4.png'

// import Swiper core and required modules
import {Pagination, Navigation} from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
//import 'swiper/css/navigation';
import 'swiper/css/pagination';
//import 'swiper/css/scrollbar';



const data = [
  {
    avatar: AVTR1,
    name: 'Chan Lee',
    review: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!'
  },
  {
    avatar: AVTR2,
    name: 'Fréderic Mourey',
    review: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!'
  },
  {
    avatar: AVTR3,
    name: 'Shatta Wale',
    review: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!'
  },
  {
    avatar: AVTR4,
    name: 'Eliane Lapoussin',
    review: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!'
  },

]
const Testimonials = () => {
  return (
    <section id='testamonials'>
      <h5>Ce que nos clients disent de nous</h5>
      <h2>Témoignages</h2>
      <Swiper className="container testimonials__container"
      modules={[Pagination]}
      spaceBetween={40}
      slidesPerView={1}
      navigation
      pagination={{ clickable: true }}
      >
        {
          data.map(({avatar, name, review}, index)=>{
            return(
              <SwiperSlide key={index} className='testimonial'>
          <div className="client__avatar">
            <img src={avatar} alt="Avatar One" />
          </div>
          <h5 className='client_name'>{name}</h5>
            <small className='client_review'>
              {review}
            </small>
        </SwiperSlide>
            )
          })
        }
        
        {/* <article className='testimonial'>
          <div className="client__avatar">
            <img src={AVTR2} alt="Avatar One" />
          </div>
          <h5 className='client_name'>Chan Lee</h5>
            <small className='client_review'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!
            </small>
        </article>
        <article className='testimonial'>
          <div className="client__avatar">
            <img src={AVTR1} alt="Avatar One" />
          </div>
          <h5 className='client_name'>Chan Lee</h5>
            <small className='client_review'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!
            </small>
        </article>
        <article className='testimonial'>
          <div className="client__avatar">
            <img src={AVTR1} alt="Avatar One" />
          </div>
          <h5 className='client_name'>Chan Lee</h5>
            <small className='client_review'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis recusandae ratione perferendis dolore doloribus harum natus temporibus a et, iusto odit dolor laborum dicta id possimus vitae modi ea! Eum!
            </small>
        </article> */}
        
        
      </Swiper>
    </section>
  )
}

export default Testimonials