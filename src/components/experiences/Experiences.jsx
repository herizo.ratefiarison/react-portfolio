import React from 'react'
import './experiences.css'
import {BsPatchCheckFill} from 'react-icons/bs'

const Experiences = () => {
  return (
    <section id='experiences'>
      <h5>Des compétences pour reussir votre projet</h5>
      <h2>Mes Compétences</h2>
      <div className="container experience__container">
        <div className="experience__frontend">
          <h3>Domaines de compétences</h3>
          <div className="experience__content">
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>DEVELOPPEMENT SPECIFIQUE</h4>
                <small className='text-light'>PHP - MYSQL - C# - JAVA</small>
              </div>
              
            </article>
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>GESTION DE PROJETS WEB</h4>
                <small className='text-light'>Site vitrine, e-commerce, application mobile</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>VALIDATION W3C, WAI-SEO</h4>
              <small className='text-light'>Accessibilité et ergonomie des pages web</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>RÉFÉRENCEMENT NATUREL</h4>
              <small className='text-light'>Des pages propre pour un referencement optimal</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>CONCEPTION GRAPHIQUE</h4>
              <small className='text-light'>Logo, templates web, plaquettes publicitaires</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>DYNAMISME DES PAGES</h4>
              <small className='text-light'>Pour embelir votre site internet et application mobile</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>ANIMATION JAVASCRIPT/AJAX</h4>
              <small className='text-light'>JQuery, VueJs, ExpressJs</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>MULTI-PLATEFORMES</h4>
              <small className='text-light'>Compatible tous supports, tablette et Applicaiton mobile</small>

              </div>
              
            </article>

          </div>

        </div>
        <div className="experience__backend">


        <h3>Competences en Developpement</h3>
          <div className="experience__content">
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>HTML/CSS</h4>
              <small className='text-light'>Experimenté</small>

              </div>
              
            </article>
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>NodeJs</h4>
              <small className='text-light'>Avancé</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>React</h4>
              <small className='text-light'>Avancé</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>Java</h4>
              <small className='text-light'>Experimenté</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>JavaScript</h4>
              <small className='text-light'>Experimenté</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>ExpressJs</h4>
              <small className='text-light'>Intermediaire</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>C#</h4>
              <small className='text-light'>Intermediaire</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>PHP/MySQL</h4>
              <small className='text-light'>Experimenté</small>

              </div>
              
            </article>

            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
              <h4>Python</h4>
              <small className='text-light'>Intermediaire</small>

              </div>
              
            </article>

          </div>


        </div>
      </div>
    </section>
  )
}

export default Experiences