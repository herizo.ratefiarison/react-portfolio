import React from 'react'
import './services.css'
import {BiCheck} from 'react-icons/bi'

const Services = () => {
  return (
    <section id='services'>
      <h5>Des prestations adaptées à vos besoins</h5>
      <h2>MES SERVICES</h2>
      <div className="container services__container">
        <article className="service">
          <div className="service__head">
            <h3>UI/UX DESIGN</h3>
          </div>
      
            <ul className='service__list'>
            <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception grapique et webdesign</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception Logo et Flyer</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception Newletter</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception spot publicitaires</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception tous supports visuels</p>
          </li>

          <li>
            <BiCheck className='service__list-icon'/>
            <p>Conception carte de visite professionnelle</p>
          </li>

            </ul>

      
        
          
          
        </article>
        {/*END UI/UX design*/}


        <article className="service">
          <div className="service__head">
            <h3>DEVELOPPEMENT WEB</h3>
          </div>
        
          <ul className='service__list'>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Création site vitrine</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Dévelopement site e-commerce</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Integration html-css respecteuses des standards web et application moderne</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Création de templates html</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Animation et dynamisation de siteweb</p>
          </li>

          <li>
            <BiCheck className='service__list-icon'/>
            <p>Responsivité des contenus web et application</p>
          </li>

          <li>
            <BiCheck className='service__list-icon'/>
            <p>Accessibilité et ergonomie des pages web</p>
          </li>

          </ul>

         
          
          
        </article>
        {/* END WEB DEVELOPMENT */}

        <article className="service">
          <div className="service__head">
            <h3>DEVELOPPEMENT SPECIFIQUE</h3>
          </div>
          <ul className='service__list'>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Création d'application mobile</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Création de progiciel</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Developpement d'application web reseau ou intrenet</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Mise en place tableau de bord utilisateur</p>
          </li>
          <li>
            <BiCheck className='service__list-icon'/>
            <p>Développement outils utilisateur</p>
          </li>

          <li>
            <BiCheck className='service__list-icon'/>
            <p>Des outils adaptés à votre environement suivant la technologie utilisée</p>
          </li>

          </ul>
          
          
        </article>

      </div>
    </section>
  )
}

export default Services