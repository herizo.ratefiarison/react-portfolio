import React from 'react'
import './about.css'
import ME from '../../assets/Aboutme.png'
import {FaAward} from 'react-icons/fa'
import {FiUsers} from 'react-icons/fi'
import {AiOutlineFundProjectionScreen} from 'react-icons/ai'

const About = () => {
  return (
    <section id='about'>
      <h5>Faisons connaissance</h5>
      <h2>Qui suis-je</h2>

      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={ME} alt="moi" />
          </div>

        </div>
        <div className="about__content">
          <div className="about__cards">
            <article className='about__card'>
              <FaAward className='about__icon'/>
              <h5>Experiences</h5>
              <small>10+ de Developpement web et appli</small>
            </article>
            <article className='about__card'>
              <FiUsers className='about__icon'/>
              <h5>Clients</h5>
              <small>20+ Clients dans le monde </small>
            </article>
            <article className='about__card'>
              <AiOutlineFundProjectionScreen className='about__icon'/>
              <h5>Projets</h5>
              <small>50+ Projets effectués </small>
            </article>
          </div>
          <p>Extrêmement motivé pour déveloper constamment mes competences et m'evoluer professionnelement. 
            Je suis serieux, dynamique, autodidacte. 
            J'aime le défi et travailler en équipe.</p>
          <a href="#contact" className='btn btn-primary'>Discutons</a>

        </div>
      </div>

    </section>
  )
}

export default About