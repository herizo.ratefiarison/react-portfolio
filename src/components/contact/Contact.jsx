import React, { useRef } from 'react'
import './contact.css'
import {MdAttachEmail} from 'react-icons/md'
import {FaFacebookMessenger} from 'react-icons/fa'
import {BsWhatsapp} from 'react-icons/bs'
import emailjs from 'emailjs-com';


const Contact = () => {

  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_klt2ooi', 'template_9jwna1q', form.current, '3q95a_WT1TnmsYOHc')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });

    e.target.reset()
  };

  return (
    <section id='contact'>
      <h5>Restons en contact</h5>
      <h2>Contactez Moi</h2>
      <div className="container contact__container">
        <div className="contact__options">
          <article className='contact__option'>
            <MdAttachEmail className='contact__option-icon'/>
            <h4>Email</h4>
            <h5>herizo.ratefiarison@gmail.com</h5>
            <a href="mailto:herizo.ratefiarison@gmail.com" target="_blank">Envoyer Message</a>
          </article>
          <article className='contact__option'>
            <FaFacebookMessenger className='contact__option-icon'/>
            <h4>Messenger</h4>
            <h5>herizo.itstation</h5>
            <a href="https://m.me/herizo.itstation" target="_blank">Envoyer Message</a>
          </article>
          <article className='contact__option'>
            <BsWhatsapp className='contact__option-icon'/>
            <h4>WhatsApp</h4>
            <h5>+261324828385</h5>
            <a href="https://api.whatsapp.com/send?phone+261324828385" target="_blank">Envoyer Message</a>
          </article>

        </div>
        <form ref={form} onSubmit={sendEmail}>
          <input type="text" name='name' placeholder='Votre nom' required/>
          <input type="email" name='email' placeholder='Votre email' required />
          <textarea name="message" rows="7" placeholder='Votre Message'></textarea>
          <button type='submit' className='btn btn-primary'>Envoyer Message</button>
        </form>
      </div>
    </section>
  )
}

export default Contact