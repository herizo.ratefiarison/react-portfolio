import React from 'react'
import {BsLinkedin, BsGithub, BsFacebook} from 'react-icons/bs'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://linkedin.com" target="__blank"><BsLinkedin/></a>
        <a href="https://github.com" target="__blank"><BsGithub/></a>
        <a href="https://facebook.com" target="__blank"><BsFacebook/></a>
    </div>
  )
}

export default HeaderSocials